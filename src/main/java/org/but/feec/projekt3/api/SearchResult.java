package org.but.feec.projekt3.api;

import javafx.beans.property.LongProperty;
import javafx.beans.property.SimpleLongProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class SearchResult {
    private LongProperty id = new SimpleLongProperty();
    private StringProperty assetName = new SimpleStringProperty();
    private StringProperty type = new SimpleStringProperty();
    private StringProperty locations = new SimpleStringProperty();
    private StringProperty customerName = new SimpleStringProperty();

    public Long getId() {
        return idProperty().get();
    }

    public void setId(Long id) {
        this.idProperty().setValue(id);
    }

    public String getAssetName() {
        return assetNameProperty().get();
    }

    public void setAssetName(String city) {
        this.assetNameProperty().setValue(city);
    }

    public String getType() {
        return typeProperty().get();
    }

    public void setType(String email) {
        this.typeProperty().setValue(email);
    }

    public String getLocations() {
        return locationsProperty().get();
    }

    public void setLocations(String givenName) {
        this.locationsProperty().setValue(givenName);
    }

    public String getCustomerName() {
        return customerNameProperty().get();
    }

    public void setCustomerName(String familyName) {
        this.customerNameProperty().setValue(familyName);
    }

    public LongProperty idProperty() {
        return id;
    }

    public StringProperty assetNameProperty() {
        return assetName;
    }

    public StringProperty typeProperty() {
        return type;
    }

    public StringProperty locationsProperty() {
        return locations;
    }

    public StringProperty customerNameProperty() {
        return customerName;
    }

    @Override
    public String toString() {
        return "SearchResult{" +
                "id=" + id +
                ", assetName=" + assetName +
                ", type=" + type +
                ", location=" + locations +
                ", customerName=" + customerName +
                '}';
    }
}
