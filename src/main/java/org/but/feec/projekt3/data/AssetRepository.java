package org.but.feec.projekt3.data;

import org.but.feec.projekt3.api.SearchResult;
import org.but.feec.projekt3.config.DataSourceConfig;
import org.but.feec.projekt3.exceptions.ResourceNotFoundException;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class AssetRepository {
    private SearchResult mapSearchResult(ResultSet rs) throws SQLException {
        SearchResult searchResult = new SearchResult();
        searchResult.setId(rs.getLong("asset_id"));
        searchResult.setAssetName(rs.getString("asset_name"));
        searchResult.setType(rs.getString("type"));
        searchResult.setLocations(rs.getString("location"));
        searchResult.setCustomerName(rs.getString("customer_name"));
        return searchResult;
    }

    public List<SearchResult> getSafeSearch(String assetName) {
        try (Connection connection = DataSourceConfig.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(
                     "SELECT * FROM public.asset a LEFT JOIN category c ON a.category_id = c.category_id LEFT JOIN location l ON a.location_id = l.location_id LEFT JOIN customer cu ON cu.customer_id = a.customer_id WHERE a.asset_name LIKE ?")
        ) {
            preparedStatement.setString(1, assetName + "%");
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                List<SearchResult> searchResults = new ArrayList<>();
                while (resultSet.next()) {
                    searchResults.add(mapSearchResult(resultSet));
                }
                return searchResults;
            }
        } catch (SQLException e) {
            throw new ResourceNotFoundException("Find Assets with name failed.", e);
        }
    }
}
