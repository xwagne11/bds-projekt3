package org.but.feec.projekt3.exceptions;

import org.but.feec.projekt3.controllers.LoginController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MyExceptions {

    private static final Logger logger = LoggerFactory.getLogger(LoginController.class);

    public static void handleException(Exception e) {
        logger.error(e.getMessage(), e);
        System.out.println("An exception occurred of type: " + e.toString());
    }
}
