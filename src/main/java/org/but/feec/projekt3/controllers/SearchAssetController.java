package org.but.feec.projekt3.controllers;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import org.but.feec.projekt3.App;
import org.but.feec.projekt3.api.PersonBasicView;
import org.but.feec.projekt3.api.SearchResult;
import org.but.feec.projekt3.data.AssetRepository;
import org.but.feec.projekt3.exceptions.MyExceptions;

import java.io.IOException;

public class SearchAssetController {

    /**@FXML
    public Button newSafeQuery;
     */
    public static String assetName;

    @FXML
    private TextField searchAssetName;

    @FXML
    void handleSafeQuery(ActionEvent event) {

        assetName = searchAssetName.getText();
        try {
            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setLocation(App.class.getResource("fxml/SearchResult.fxml"));
            Scene scene = new Scene(fxmlLoader.load(), 1050, 600);
            Stage stage = new Stage();
            stage.setTitle("BDS Wagner Project 3 Safe Search");
            stage.setScene(scene);

            stage.show();
        } catch (IOException e) {
            MyExceptions.handleException(e);
        }
    }
}
