package org.but.feec.projekt3.controllers;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import org.but.feec.projekt3.App;
import org.but.feec.projekt3.api.PersonBasicView;
import org.but.feec.projekt3.api.SearchResult;
import org.but.feec.projekt3.data.AssetRepository;
import org.but.feec.projekt3.exceptions.MyExceptions;
import org.but.feec.projekt3.controllers.SearchAssetController;

import java.util.List;


public class SearchResultController {

    @FXML
    private TableColumn<SearchResult, Long> assetId;
    @FXML
    private TableColumn<SearchResult, String> assetNames;
    @FXML
    private TableColumn<SearchResult, String> type;
    @FXML
    private TableColumn<SearchResult, String> locations;
    @FXML
    private TableColumn<SearchResult, String> customerName;

    @FXML
    private TableView<SearchResult> systemAssetTableView;

    public SearchResultController(){
    }
    @FXML
    private void initialize(){
        String assetName = SearchAssetController.assetName;

        List<SearchResult> search = new AssetRepository().getSafeSearch(assetName);
        assetId.setCellValueFactory(new PropertyValueFactory<SearchResult, Long>("id"));
        assetNames.setCellValueFactory(new PropertyValueFactory<SearchResult, String>("assetName"));
        type.setCellValueFactory(new PropertyValueFactory<SearchResult, String>("type"));
        locations.setCellValueFactory(new PropertyValueFactory<SearchResult, String>("locations"));
        customerName.setCellValueFactory(new PropertyValueFactory<SearchResult, String>("customerName"));

        ObservableList<SearchResult> observableSearchList = FXCollections.observableArrayList(search);
        systemAssetTableView.setItems(observableSearchList);

    }

}

