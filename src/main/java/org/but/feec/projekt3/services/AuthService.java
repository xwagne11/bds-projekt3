package org.but.feec.projekt3.services;

import at.favre.lib.crypto.bcrypt.BCrypt;
import org.but.feec.projekt3.api.PersonAuthView;
import org.but.feec.projekt3.data.PersonRepository;
import org.but.feec.projekt3.exceptions.ResourceNotFoundException;

import java.util.Arrays;

public class AuthService {

    private PersonRepository personRepository;

    public AuthService(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }

    private PersonAuthView findPersonByEmail(String email) {
        return personRepository.findPersonByEmail(email);
    }

    public boolean authenticate(String username, char[] password) {
        if (username == null || username.isEmpty() || password == null || password.length == 0) {
            Arrays.fill(password, '*');
            return false;
        }

        PersonAuthView personAuthView = findPersonByEmail(username);
        if (personAuthView == null) {
            throw new ResourceNotFoundException("Provided username was not found.");
        }

        BCrypt.Result result = BCrypt.verifyer().verify(password, personAuthView.getPassword());
        Arrays.fill(password, '*');
        return result.verified;
    }


}
